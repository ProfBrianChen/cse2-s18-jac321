//James Austin Conte CSE2 2/20/18
//creates an argyle pattern based on dimesions specified by the user

import java.util.Scanner;
public class Argyle{
                // main method required for every Java program
               public static void main(String[] args) {
                 Scanner myScanner = new Scanner( System.in );
                 int wid = 0; 
                 boolean y = false; //create variables for evaluating the while loop for verification
 while(y== false || wid < 1 ){
   System.out.print("please enter a positive integer for the width of Viewing window in characters. "); 
    y = myScanner.hasNextInt();
    if(y == true){
 
   wid = myScanner.nextInt();
 }else{
      String junkWord = myScanner.next();
    }
 }
                 int h = 0; 
                 boolean x = false; //create variables for evaluating the while loop for verification
 while(x== false || h < 1 ){
   System.out.print("please enter a positive integer for the height of Viewing window in characters.  "); 
    x = myScanner.hasNextInt();
    if(x == true){
 
   h = myScanner.nextInt();
 }else{
      String junkWord = myScanner.next();
    }
 }
                 int widd = 0; 
                 boolean a = false; //create variables for evaluating the while loop for verification
 while(a== false || widd < 1 ){
   System.out.print("please enter a positive integer for the width of the argyle diamonds. "); 
    a = myScanner.hasNextInt();
    if(a == true){
 
   widd = myScanner.nextInt();
 }else{
      String junkWord = myScanner.next();
    }
 }
                 int cen = 0; 
                 boolean z = false; //create variables for evaluating the while loop for verification
 while(z== false || cen < 1 || (cen % 2) == 0 || cen > (widd/2)){
   System.out.print("please enter a positive odd integer for the width of the argyle center stripe. "); 
    z = myScanner.hasNextInt();
    if(z == true){
 
   cen = myScanner.nextInt();
 }else{
      String junkWord = myScanner.next();
    }
 }
   System.out.print("please enter a first character for the pattern fill. "); //any value will be accepted for the following fields, thus no verification is required
        String t = myScanner.next();
        char fir = t.charAt(0);
   System.out.print("please enter a second character for the pattern fill. "); 
        String te = myScanner.next();
        char sec = te.charAt(0);
   System.out.print("please enter a third character for the stripe fill. "); 
        String tem = myScanner.next();
        char str = tem.charAt(0);
                 int d = 0;              //control variables for creating the pattern
                 int de = 0;
                 int pr = 0;
                 int pri = 0;
                 int k = 0;
                 int widOr = wid;
                while(h>0){                   //determines how long each iteration of the pattern will last based on the dimensions
                  if (widd * 2 > h){
                    d = h;
                  } else{
                    d = widd * 2;
                  }
                  
                for(int i = 0; i < d; i++){ //for loop for height
                  while(wid>0){       //same fucntion as h while loop but for length
                  if (widd * 2 > wid){
                    de = wid;
                  } else{
                    de = widd * 2;
                  }
                   for(int j = 1; j <= de; j++){ //for loop for length
                     for( k = 0; k < cen ; k++){
             if(j == i +k || j == (widd *2) - (i + k - 1)){ //statements for printing stripes
               System.out.print(str);
               pr = 1;
               break;
             }
       
                     }
              if(i != 0 && pr != 1 && i <= widd){  //statements for printing diamond
             for( int l = 0; l < i; l++ ){
               if(j == widd - l || j == widd + 1 + l){
                 System.out.print(sec);
                 pri = 1;
                 break;
               }
             }        
              }else if( i > widd && pr != 1){  //statements for printing background
                for(int l = 0; l < (widd *2) - i; l ++){
                  if(j == widd - l || j == widd + 1 + l){
                 System.out.print(sec);
                 pri = 1;
                 break;              
                }
                }
              }
              if(pr == 1 || pri == 1){  //resets controlls for stripes and diamonds
                  pr =0;
                  pri = 0;
              } else{
               System.out.print(fir);
              }
                
}
               wid -= widd * 2;  //adjust remaining width
                  }
              System.out.println();
                  wid = widOr;
                 }
                  h -= widd * 2; //adjust remaining height
               }
               }
}
 