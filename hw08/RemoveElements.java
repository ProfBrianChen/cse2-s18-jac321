//James Austin Conte CSE2 4/9/18
//adds methods to the given code for creating an array, removing an item from a position in the array, and removing all items with a specified value form the array
import java.util.Scanner; //added methods begin at line 49
public class RemoveElements{
  public static void main(String [] arg){
    Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
    String answer="";
    do{
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);
 
      System.out.print("Enter the index ");
      index = scan.nextInt();
      newArray1 = delete(num,index);
      String out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
 
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2="The output array is ";
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);
       
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next();
    }while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
    String out="{";
    for(int j=0;j<num.length;j++){
      if(j>0){
        out+=", ";
      }
      out+=num[j];
    }
    out+="} ";
    return out;
  }
  public static int[] randomInput(){ //method for generating an array of random values
    int[] a = new int[10];
    for(int i = 0; i < 10; i ++){
      a[i] = (int) (Math.random() * 10);
    }
    return a;
  }
  public static int[] delete(int[] list, int pos){ //method for deleting an entry in the array based on position
    if(pos > -1 && pos < list.length){
  int[] nlist = new int[list.length-1];
    int c = 0;
  for(int i = 0; i < list.length; i++){ //loop for creating modified array
    if(i == pos){
      c = 1;
    } else{
   nlist[i-c] = list[i];
    }
  }
    return nlist;
  }
    return list;
  }
  public static int[] remove(int[] list, int target){ //method for deleting all entries with a specified value 
    int c = 0;
  for(int i = 0; i < list.length; i++){ //loop for determining how many items must be deleted, and how big the new array must be.
    if(list[i] == target){
      c++;
    } 
  }
    int[] nlist = new int[list.length-c];
    int c2 = 0;
    for(int i = 0; i < list.length; i++){ //loop for creating the modified array
    if(list[i] == target){
      c2 ++;
    } else{
   nlist[i-c2] = list[i];
    }
  }
    return nlist;
  }
}
