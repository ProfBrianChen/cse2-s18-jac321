//James Austin Conte CSE2 4/9/18
//creates an array of values determined by the user and then performs binary and linear searches respetively before and after scrambling the array
import java.util.Scanner;
import java.util.Random;
public class CSE2Linear{
                // main method required for every Java program
               public static void main(String[] args) {
                 int[] grades = new int[15]; //intiate array for the students
                 int i = 0;
                 Scanner myScanner = new Scanner ( System.in );
                 Random randomGenerator = new Random();
                 System.out.println("Enter 15 ascending ints for final grades in CSE2: ");
                 while(i < 15){ //loop for filling first array
                   boolean a = myScanner.hasNextInt();
                   if(a == true){ //check that input is an int
                     int w = myScanner.nextInt();
                     if(w < 0 || w > 100){  //check that int is within parameters
                       System.out.println("Error, number out range 0-100");
                     } else if(i != 0 && w < grades[i-1]){
                       System.out.println("Error, numbers are not in increasing order");  
                     } else{
                      grades[i] = w;
                      i++;
                     }
                   } else{
                     System.out.println("Error, not an integer");
                     String junkword = myScanner.next();
                   }
                  } 
                 int s = 0;
                 boolean y = false;
                 while(y == false){ //loop for determining correct input
                   System.out.println("Enter a grade to search for: ");
                   boolean a = myScanner.hasNextInt();
                   if(a == true){
                    y = true;
                    s = myScanner.nextInt();
                   }else{
                     String juckWord = myScanner.next();
                     System.out.println("Error, not an integer");
                   }
                 }
                 int c = 0; //vaiables for binary search
                 double d = 0.5;
                 double de = d;
                 while(c <4){ //loop for binary search
                   System.out.println(d);
                   System.out.println(grades[(int) (15 * d)]);
                   if(s == grades[(int) (15 * d)]){
                     System.out.println(s + " was found with " + (c+1) + " iterations");
                     break;
                   } else if(s > grades[(int) (15 * d)]){
                     d += de/2;
                   } else{
                     d -= de/2;
                   }
                   de = de/2;
                   if(c == 3){
                     System.out.println(s + " was not found with " + (c+1) + " iterations");
                   }
                   c++;
                 }
                   
                 for(int j = 0; j < 15; j ++){ //loop for scrambling array
                   int r = randomGenerator.nextInt(15);
                   int h = grades[j];
                   grades[j] = grades[r];
                   grades[r] = h;
                 }
                 System.out.println("Scrambled: ");
                 for(int j = 0; j < 15; j ++){ //loop for printing values of the scrambled arry
                   System.out.println(grades[j]);
                 }
                 int v = 0;
                 boolean z = false;
                 while(z == false){ //loop for determining correct input
                   System.out.println("Enter a grade to search for: ");
                   boolean a = myScanner.hasNextInt();
                   if(a == true){
                    z = true;
                    v = myScanner.nextInt();
                   }else{
                     String juckWord = myScanner.next();
                     System.out.println("Error, not an integer");
                   }
                 }
                 for(int j = 0; j < 15; j ++){ //loop for executing linear search
                   if(v == grades[j]){
                     System.out.println(v + " was found with " + (j+1) + " iterations");
                     break;
                   } else if(j == 14){
                     System.out.println(v + " was not found with " + (j+1) + " iterations");
                   }
                 }
               }
}
