//James Austin Conte CSE2 2/9/18
//Calculates total cubic miles of rainfall in a given area
import java.util.Scanner;
public class Convert{
                // main method required for every Java program
               public static void main(String[] args) {
Scanner myScanner = new Scanner( System.in );
System.out.print("Enter the affected area in acres: ");
double area = myScanner.nextDouble();
System.out.print("Enter the rainfall in the affected area (in): " );
double inches = myScanner.nextDouble();
double gallons = 27154; // number of gallons from an inch of rain over 1 acre
double totalGallons = inches * area * gallons;
double totalMiles = totalGallons * 9.0817e-13;
System.out.println(totalMiles + " cubic miles");
               }
}