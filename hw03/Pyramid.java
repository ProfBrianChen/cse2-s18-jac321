//James Austin Conte CSE2 2/9/18
//Calculates volume of a pyramid
import java.util.Scanner;
public class Pyramid{
                // main method required for every Java program
               public static void main(String[] args) {
Scanner myScanner = new Scanner( System.in );
System.out.print("The square side of the pyramid is (input length) : ");
double length = myScanner.nextDouble();
System.out.print("The height of the pyramid is (input height) : " );
double height = myScanner.nextDouble();
double base = Math.pow(length , 2);
double volume = (base * height)/3;
System.out.println("The volume inside the pyramid is " + volume);
               }
}
  