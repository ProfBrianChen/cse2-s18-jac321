//James Austin Conte CSE2 4/13/18
//creates three arrays and adds them together
import java.util.Random;
public class lab10{
                // main method required for every Java program
               public static void main(String[] args) {
               Random r = new Random();
               int r1 = r.nextInt(9) + 1;
               int r2 = r.nextInt(9) + 1;
               int[][] A = increasingArray(r1, r2, true);
               int[][] B = increasingArray(r1, r2, false);
               int[][] C = increasingArray(r.nextInt(9) +1, r.nextInt(9)+1, true);
               System.out.println("A");
               printArray(A, true);
               System.out.println();
               System.out.println("B");
               printArray(B, false);
               System.out.println();
               System.out.println("C");
               printArray(C, true);
               System.out.println();
               System.out.println("A + B");
               printArray(addMatrix(A,true,B,false), true);
               System.out.println();
               System.out.println("A + C");
               printArray(addMatrix(A,true,C,true), true);
               }
               public static int[][] increasingArray(int height, int width, boolean format){
               int[][] array = new int[height][width];
               int h = 1;
               if(format == true){
               for(int x = 0; x < height; x++){
               for(int y = 0; y < width; y++){
               array[x][y] = h;
               h ++;
               }
               }
               } else{
               for(int y = 0; y < width; y++){
               int i = h;
               for(int x = 0; x < height; x++){
               array[x][y] = i;
               i ++;
               }
               h+=height;
               }
               }
               return array;
               }
               public static void printArray(int[][] array, boolean format){
               if(array == null){
                 System.out.println("the array was empty");
                 return;
               }
               if(format == true){
               for(int x = 0; x < array.length; x++){
               for(int y = 0; y < array[0].length; y++){
               System.out.print(array[x][y] + " ");
               }
               System.out.println();
               }
               } else{
               for(int y = 0; y < array[0].length; y++){
               for(int x = 0; x < array.length; x++){
               System.out.print(array[x][y] + " ");
               }
               System.out.println();
               }
               }
               }
               public static int[][] translate(int[][] array){
                int h = 1;
                int[][] array2 = new int[array.length][array[0].length];
                for(int x = 0; x < array.length; x++){
                for(int y = 0; y < array[0].length; y++){
                  array2[x][y] = h;
                  h ++;
               }
               }
               return array2;
               }
               public static int[][] addMatrix(int[][] a, boolean formata, int[][] b, boolean formatb){
               if(a.length != b.length || a[0].length != b[0].length){
                 System.out.println("The arrays cannot be added!");
                 return null;
               } 
               if(formata == false){
                 a = translate(a);
               }
               if(formatb == false){
                 b = translate(b);
               }
                int[][] array2 = new int[a.length][a[0].length];
                for(int x = 0; x < a.length; x++){
                for(int y = 0; y < a[0].length; y++){
                  array2[x][y] = a[x][y] + b[x][y];
               }
               }
               return array2;
               }
}