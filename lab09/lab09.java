//James Austin Conte CSE2 4/13/18
//creates an array and manipulates it in various ways
public class lab09{
                // main method required for every Java program
               public static void main(String[] args) {
                int[] array0 = {1,2,3,4,5,6,7,8,9}; //create the initial array
                int[] array1 = copy(array0);
                int[] array2 = copy(array0);
                print(array0);
                inverter(array0);
                print(array0);
                inverter2(array1);
                print(array1);
                int[] array3 = inverter2(array2);
                print(array3);
               }
  public static int[] copy(int[] array){ //copys a given array
    int[] narray = new int[array.length];
    for(int i = 0; i < array.length; i++){
      narray[i] = array[i];
    }
    return narray;
  }
  public static void inverter(int[] array){ //inverts a given array
    int[] narray = new int[array.length];
    for(int i = 0; i < array.length; i++){
    narray[i] = array[array.length - (i +1)];
    }
     for(int i = 0; i < array.length; i++){
    array[i] = narray[i];
    }
  }
  public static int[] inverter2(int[] array){ //copys a given array and inverts the copy
    int[] narray = copy(array);
    inverter(narray);
    return narray;
  }
  public static void print(int num[]){ //prints an array
    System.out.print("{ ");
    for(int j=0;j<num.length;j++){
      if(j>0){
        System.out.print(", ");
      }
      System.out.print(num[j]);
    }
    System.out.println("} ");
  }
  }