//James Austin Conte CSE2 4/17/18
//simulates a simplified poker game
import java.util.Random;
public class DrawPoker{
                // main method required for every Java program
               public static void main(String[] args) {
                Random randomGenerator = new Random();
                int[] deck = new int[51];
                for(int j = 0; j < 51; j ++){ //loop for creating deck array
                   deck[j] = j;
                }
                for(int j = 0; j < 51; j ++){ //loop for scrambling deck array
                   int r = randomGenerator.nextInt(51);
                   int h = deck[j];
                   deck[j] = deck[r];
                   deck[r] = h;
                }
               
               int[] h1 = new int[5]; //create arrays for hands
               int c1 = 0;
               int[] h2 = new int[5];
               int c2 = 0;
               for(int j = 0; j < 10; j++){ //deals cards to each hand
                if(j % 2 == 0){
                 h1[c1] = deck[j];
                 c1++;
                }else{
                 h2[c2] = deck[j];
                 c2++;
                }
               }
               System.out.print("Player 1: "); //prints hand for player 1
               for(int i = 0; i < 5; i++){
                int p = h1[i] % 13;
                switch(h1[i] % 13){
                 case 10:
                   System.out.print("J ");
                   break;
                 case 11:
                   System.out.print("Q ");
                   break;
                 case 12:
                   System.out.print("K ");
                   break;
                 case 0:
                   System.out.print("A ");
                   break;
                 default:
                   System.out.print((p +1)+ " ");
                 }
               }
               System.out.println();
               System.out.print("Player 2: "); //prints hand for player 2
               for(int i = 0; i < 5; i++){
                int p = h2[i] % 13;
                switch(h2[i] % 13){
                 case 10:
                   System.out.print("J ");
                   break;
                 case 11:
                   System.out.print("Q ");
                   break;
                 case 12:
                   System.out.print("K ");
                   break;
                 case 0:
                   System.out.print("A ");
                   break;
                 default:
                   System.out.print((p +1)+ " ");
                }
               }
               System.out.println();
               int score1 = 0; //intiate score for each player. Note the numbers beloware simply for determining the relative value of a 
               int score2 = 0;
               if(pair(h1) == true && three(h1) == false){ //determines score for player 1
                System.out.println("Player 1 has a pair ");
                score1 = 19;
               }
               else if(three(h1) == true && full(h1) == false){
                System.out.println("Player 1 has three of a kind ");
                score1 = 29;
               }
               else if(full(h1) == true){
                System.out.println("Player 1 has a full house ");
                score1 = 49;
               }
               else if(flush(h1) == true){
                 System.out.println("Player 1 has a flush");
                 score1 = 39;
               } else {
                score1 = max(h1);
               }
               if(pair(h2) == true && three(h2) == false){ //determines score for player 2
                System.out.println("Player 2 has a pair ");
                score2 = 19;
               }
               else if(three(h2) == true && full(h2) == false){
                 System.out.println("Player 2 has three of a kind ");
                 score2 = 29;
               }
               else if(full(h2) == true){
                System.out.println("Player 2 has a full house ");
                score2 = 49;
               }
               else if(flush(h2) == true){
                 System.out.println("Player 2 has a flush");
                 score2 = 39;
               } else {
                 score2 = max(h2);
               }
               if(score2 > score1){ //determines who wins
                 System.out.println("Player 2 wins");
               } else if(score1 > score2){
                 System.out.println("Player 1 wins");
               }else{
                 System.out.println("Draw");
               }
               }
  public static boolean pair(int[] h){ //determines a pair
    for(int i = 0; i < 5; i++){
      for(int j = 0; j<5; j++){
        if(i != j){
         if(h[i]  % 13 == h[j] % 13){
           return true;
         } 
        }
      } 
  }
    return false;
  }
  public static boolean three(int[] h){ //determines three of a kind
    int c = 0;
    for(int i = 0; i < 5; i++){
      for(int j = 0; j<5; j++){
        if(i != j){
         if(h[i] % 13 == h[j] % 13){
           c++;
         }  
        }
        if(c == 2){
        return true;  
        }
      }
      c = 0;
    }
    return false;
  }
  public static boolean full(int[] h){  //determines a full house
    int c = 0;
    int d = -1;
    int s = 0;
    for(int i = 0; i < 5; i++){
      for(int j = 0; j<5; j++){
        if(i != j){
         if(h[i] % 13== h[j]  % 13 && d == -1){
           c++;
           s = i;
         }
         if(h[i] % 13== h[j] % 13 && i != s){
           c++;
           if(c == d){
             return true;
           }
         }
        }
      }
      if(c == 1){
       d = 2;
      } else if(c ==2){
       d = 1;
      } else{
       return false;
      }
      c = 0;
    }
return false;
  }
  public static boolean flush(int[] h){ //determines a flush
    int c = 0;
    for(int i = 1; i < 5; i++){
      if((int)(h[i]/13) == (int)(h[0]/13)){
        c++;
      }
    }
    if(c == 4){
      return true;
    }else{
      return false;
    }
    }
  public static int max(int[] h){ //deterimines the highest value in a hand
    int m = 0;
    for(int i = 1; i < 5; i++){
      if(h[i] % 13 > m){
        m = h[i] % 13;
      }
    }
    return m;
  }
}