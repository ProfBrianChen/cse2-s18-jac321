//James Austin Conte CSE2 2/20/18
//Yahtzee simulator
import java.util.Scanner;
public class Yahtzee{
                // main method required for every Java program
               public static void main(String[] args) {
                 int[] rolls = new int[5]; //intiate array for the rolls
                 int[] counter = new int[6]; //initiate array for totaling the number of each roll(i.e. 3 rolls of 6 will generate a 3 in the 6th slot of the array)
                 Scanner myScanner = new Scanner( System.in );
System.out.print("Would you like to roll randomly or to type in a 5 digit number representing the result of a specific roll? (Type 1 to enter a number) ");
int ans = myScanner.nextInt();
if(ans == 1){ //sorts the user determined roll and ensures that it is a possible roll
 System.out.print("enter a number to represent the dice rolls " );
  double r = myScanner.nextDouble();
  for(int i = 0; i < 5; i++){
    rolls[i] = (int) (r % 10);  
    if((r % 10) > 6  || (r/66666) > 1 || (r % 10) == 0){ //checks error conditions
      System.out.println("error: invalid number" );
      break;
    }
    r = (int) (r / 10);
  }
}else{
  for(int j = 0; j < 5; j++){ //if no user number is entered, gerenates a random roll
   int val = (int) (Math.random() * 6) + 1;
    rolls[j] = val; 
    System.out.println(rolls[j]);
    
  }
}
 double score1 = 0;  
 for(int k = 0; k < 5; k++){  //computes score 1 (upper score) by totaling the values of all the dice
    score1 += rolls[k];
    counter[rolls[k]-1] ++; //stores the number of each roll in counter
 }
 
 int bonus = 0;  //add bonus if applicable (note that given the current rules and a single role, this should never trigger, with the maximum value being 30)
 if(score1 >= 63){
   bonus += 35;
   
 }
 int score2 = 0;
 int count1 = 0;
 int count2 = 0;
 int count3 = 0;
 int mult = 0;
 for(int i = 0; i < 6; i++){
  // computes score2 (lower score) by evaluating the values of the counter array and checking for required combinations
  switch(counter[i]){
    case 1: //determines straights
      if (i == 5){
        if(counter[i-1] == 1 ){
          count1+= 1;
        }
      }else if(i == 0){
        if(counter[i+1] == 1){
          count1+=1;
        }
      } else if ((counter[i-1] == 1) && (counter[i+1] == 1)){
      count1 += 1;
      }
      break;
    case 2: //determines a pair
      count2 = 1; 
      break;
    case 3: //determines three of a kind
      count3 = 1;
      mult = i+1;
      break;
    case 4: //adds four of a kind
      score2 += 4 * (i+1);
      break;
    case 5: //adds yahtzee
      score2 += 50;
      break;
  }
 }
   if(count1 == 4){ //adds small straight
     score2 += 30;
   }
   if(count1 == 5){ //adds large straight
     score2 += 40;
   }
   if(count2 == 1 && count3 == 1){ //adds fullhouse
     score2 += 25;
   } else if(count3 == 1){ //adds three of a kind
     score2 += 3 * mult;
   }
    score2 += (score1); //chance is calculated in the same manner as score1, therefore the calculation is simplfied by adding score1 to score2 prior to totaling, then adding again for the grand total
    //prints final score values
    System.out.println("upper section initial score: " + score1);
    System.out.println("upper section total score: " + (score1 + bonus));
    System.out.println("lower section score: " + score2);
    System.out.println("grandtotal: " + (score1 + score2 + bonus));
    
 }              
}
                 
                 
          
                 
                 
                 
                 
                 