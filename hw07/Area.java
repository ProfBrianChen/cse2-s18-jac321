//James Austin Conte CSE2 3/26/18
//finds the area of a user specified shape
import java.util.Scanner;
public class Area{
                // main method required for every Java program
               public static void main(String[] args) {               
                 Scanner myScanner = new Scanner( System.in );
                 boolean y = false;
                 while(y == false){ //loop for determining correct input and type of area to calculate
                   System.out.println("Please enter a shape to find its area (rectangle, triangle, or circle)");
                   boolean a = myScanner.hasNext("circle");
                   boolean b = myScanner.hasNext("rectangle");
                   boolean c = myScanner.hasNext("triangle");
                   if(a == true || b == true || c == true){
                    y = true;
                    
                   }else{
                     String juckWord = myScanner.next();
                   }
                   if(a == true){
                     cir();
                   } else if(b == true){
                     rec();
                   } else if(c == true){
                     tri();
                   }
                 }
               }
               public static void cir(){ //method for circles
                   double rad = 0;
                   Scanner myScanner = new Scanner( System.in );    
                   rad = check("radius");
                   System.out.println( 3.14 * Math.pow(rad, 2));
               }                 
               public static void rec(){ //method for rectangles
                   double h = 0;
                   double len = 0;
                   Scanner myScanner = new Scanner( System.in );    
                   len = check("length");
                   h = check("height");
                   System.out.println("Area = " + len * h);
               }     
               public static void tri(){ //method for triangles
                   double h = 0;
                   double len = 0;
                   Scanner myScanner = new Scanner( System.in );    
                   len = check("base");
                   h = check("height");
                   System.out.println("Area = " + (len * h)/2);
               }     
               public static double check(String dim){ // method for checking correct numerical input
                boolean y = false;
                double x = 0;
                Scanner myScanner = new Scanner( System.in );
                while(y == false){
                    System.out.println("Please enter the " + dim);              
                    boolean z = myScanner.hasNextDouble();
                    if(z == true){
                      y = z;
                      x = myScanner.nextDouble();
        
                    } else{
                      String junkword = myScanner.next();
                   }
                } 
                return x;
               }
}
