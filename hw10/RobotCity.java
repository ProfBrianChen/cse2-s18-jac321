//James Austin Conte CSE2 4/13/18
//creates an array and then manipulates ranodmly selected values
import java.util.Random;
public class RobotCity{
                // main method required for every Java program
               public static void main(String[] args) {
                Random r = new Random();
                 int[][] cityArray = buildCity(); //create initial array
                 System.out.println("Initial array");
                display(cityArray);
                invade(cityArray, r.nextInt(25) +1);
                System.out.println("Invasion start");
                display(cityArray);
                for(int i = 0; i < 5; i++){ //loop for updating invasion
                System.out.println("Update " + (i +1));
                update(cityArray);
                display(cityArray);
                }
               }
               public static int[][] buildCity(){ //loop for creating an array
               Random r = new Random();
               int[][] cityArray = new int[r.nextInt(6) + 10][r.nextInt(6) + 10];
               for(int i = 0; i < cityArray.length; i++){
                 for(int j = 0; j < cityArray[i].length; j++){
                   cityArray[i][j] = r.nextInt(900) + 100;
                 }
               }
               return cityArray;
               }
               public static void display(int[][] array){ //displays current status of the array
               for(int i = array.length-1; i > -1; i--){
                 for(int j = 0; j < array[i].length; j++){
                   System.out.printf("%4d ", array[i][j]);
                 }
                 System.out.println();
               }
                 System.out.println();
               }
               public static void invade(int[][] array, int n){ //starts the invasion
                 Random r = new Random();
                 for(int i = 0; i < n; i++){
                  int x =r.nextInt(array[0].length);
                  int y = r.nextInt(array.length);
                  if(array[y][x] / -1 > 0){
                    i--;
                  } else{
                   array[y][x] *= -1;
                 }
               }
               }
               public static void update(int[][] array){ //updates the invasion
                 Random r = new Random();
                 for(int i = 0; i < array.length; i++){
                   int b = 0;
                   for(int j = 0; j < array[i].length; j++){
                     if(array[i][j] / -1 > 0){
                       if(b == 0){
                      array[i][j] *= -1;
                       }
                       else{
                         b = 0;
                       }
                     if(j < array[i].length -1){
                       j++;
                       if(array[i][j] / -1 > 0){
                        j--;
                        b = 1;
                       } else{
                       array[i][j] *= -1;
                       }
                     }
                 }
                   }
                  
               }
               }
}