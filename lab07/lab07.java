//James Austin Conte CSE2 3/6/18
//generates a random sentence

import java.util.Scanner;
import java.util.Random;
public class lab07{
                // main method required for every Java program
               public static void main(String[] args) {
                final String sub = sen(); //calls the method for the topic sentence
                 Random randomGenerator = new Random();
                 int randomInt = randomGenerator.nextInt(4); //determines the number of times the second method will be called
                 for(int x = 0; x <= randomInt; x++){
                   act(sub); //generates action sentences
                 }
                 con(sub); //generates conclusion sentence

               }
                public static void con(String fnoun){ //conclusion sentence method
                  System.out.println("The " + fnoun + " " + verbm() + " its " + nouna() + "!");
                }
                public static void act(String fnoun){ //action sentence method
                  String sub = "";
                  Random randomGenerator = new Random();
                  int randomInt = randomGenerator.nextInt(3);
                  if(randomInt == 2){
                    sub = "It";
                  } else{
                    sub = "The " + fnoun;                 
                  }
                   System.out.println(sub + " used " + nouna() + " and " + verbm() + " the " + adjm("") + " " + nounm(fnoun));
                   
                }
                   
               public static String sen(){ //subject sentnece method
               boolean y = true;
               Scanner myScanner = new Scanner( System.in ); 
               String fnounf = "";
     while(y == true){
                String fnoun = "";
                String fadj = "";
                String snoun = "";
                String sadj = "";
                String tadj = "";
                String verb = verbm(); 
               nounm(fnoun);
               fnoun = nounm(fnoun);
               nounm(fnoun);
               snoun = nounm(fnoun);
               adjm(fadj);
               fadj = adjm(fadj);
               adjm(fadj);
               sadj = adjm(fadj);
               adjm(sadj);
               tadj = adjm(sadj);

               System.out.println("The "+ fadj+ " " + sadj+ " " + fnoun + " " + verb+ " " + "the" + " "+ tadj+ " " + snoun);
               System.out.print("Would you like another topic sentence? "); 
               boolean z = myScanner.hasNext("yes");
               if(z==true){
               String junkWord = myScanner.next();
               } else{
                 y = z;
               }
                fnounf = fnoun;
               }
               return fnounf;
               }
  public static String nounm(String fnoun){ //generates subject noun
String nnoun = "";
int count =0;
while(count == 0 || fnoun == nnoun){
count = 1;
Random randomGenerator = new Random();
int randomInt = randomGenerator.nextInt(9);
switch (randomInt){
  case 1: 
    nnoun = "cat";
    break;
   case 2: 
    nnoun = "dog";
    break;
    case 3: 
    nnoun = "fish";
    break;
    case 4: 
    nnoun = "cow";
    break;
    case 5: 
    nnoun = "sheep";
    break;
    case 6: 
    nnoun = "elephant";
    break;
    case 7: 
    nnoun = "ant";
    break;
    case 8: 
    nnoun = "pig";
    break;
    default: 
    nnoun = "tiger";
    break;
}
}
return nnoun;
  }
 public static String adjm(String fadj){ //method for generating adjectives
     
Random randomGenerator = new Random();
String adj = "";
int count =0;
while(count == 0 || fadj == adj){
int randomInt = randomGenerator.nextInt(9); 
count = 1;
switch (randomInt){
  case 1: 
    adj = "big";
    break;
   case 2: 
    adj = "ferocious";
    break;
    case 3: 
    adj = "hungry";
    break;
    case 4: 
    adj = "tired";
    break;
    case 5: 
    adj = "fat";
    break;
    case 6: 
    adj = "wild";
    break;
    case 7: 
    adj = "curious";
    break;
    case 8: 
    adj = "intimidating";
    break;
    default: 
    adj = "angry";
    break;
}
}
   return adj;
  }
 public static String verbm(){ //method for generating verbs
     
Random randomGenerator = new Random();
String verb = "";
int randomInt = randomGenerator.nextInt(8);
switch (randomInt){
  case 1: 
    verb = "approached";
    break;
   case 2: 
    verb = "ate";
    break;
    case 3: 
    verb = "saw";
    break;
    case 4: 
    verb = "attacked";
    break;
    case 5: 
    verb = "chased";
    break;
    case 6: 
    verb = "noticed";
    break;
    case 7: 
    verb = "discovered";
    break;
    
    default: 
    verb = "heard";
    break;
}
return verb;
  }                     
 public static String nouna(){ //method for generating object nouns
String nnoun = "";
Random randomGenerator = new Random();
int randomInt = randomGenerator.nextInt(9);
switch (randomInt){
  case 1: 
    nnoun = "boxes";
    break;
   case 2: 
    nnoun = "fangs";
    break;
    case 3: 
    nnoun = "strength";
    break;
    case 4: 
    nnoun = "books";
    break;
    case 6: 
    nnoun = "speed";
    break;
    case 7: 
    nnoun = "trees";
    break;
    case 8: 
    nnoun = "maps";
    break;
    default: 
    nnoun = "water";
    break;
}
return nnoun;
  }                
}             
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 